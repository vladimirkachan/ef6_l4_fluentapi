﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.Data
{
    public class Attendee
    {
        public int AttendeeTrackingID {get; set;}
        public string LastName { get; set; }
        public DateTime? DateAdded { get; set; }

    }
}
