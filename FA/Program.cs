﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FA.DataAccess;
using FA.DataAccess.Context;

namespace FA
{
    class Program
    {
        static void Main(string[] args)
        {
            Database.SetInitializer(new DropCreateAttendeeDb());
            using CodeContext db = new();
            var q = from a in db.Attendees select a;
            Console.WriteLine(q);
            Console.WriteLine($"Count = {q.Count()}");
            foreach(var i in q)
                Console.WriteLine($"{i.AttendeeTrackingID}.{i.LastName} <{i.DateAdded}>");

            Console.ReadKey();
        }
    }
}
