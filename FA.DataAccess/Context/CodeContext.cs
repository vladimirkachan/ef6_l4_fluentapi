﻿using System;
using System.Data.Entity;
using System.Linq;
using FA.Data;
using FA.DataAccess.Configuration;

namespace FA.DataAccess.Context
{
    public class CodeContext : DbContext
    {
        public DbSet<Attendee> Attendees {get; set;}    
        public CodeContext() : base("data source=.; initial catalog=FAdb; integrated security=True; MultipleActiveResultSets=True; App=EntityFramework") {}
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Configurations.Add(new AttendeeSplittedEntityConfig()); 
        }
    }
}