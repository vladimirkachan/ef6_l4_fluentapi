﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FA.Data;
using FA.DataAccess.Context;

namespace FA.DataAccess
{
    public class DropCreateAttendeeDb : DropCreateDatabaseIfModelChanges<CodeContext>
    {
        protected override void Seed(CodeContext context)
        {
            base.Seed(context);
            context.Attendees.Add(new Attendee {DateAdded = DateTime.Now, LastName = "KachaN"});
            context.Attendees.Add(new Attendee {DateAdded = DateTime.Now, LastName = "Malinova"});
            context.Attendees.Add(new Attendee {DateAdded = DateTime.Now, LastName = "Shevchuk"});
            context.SaveChanges();
        }
    }
}
