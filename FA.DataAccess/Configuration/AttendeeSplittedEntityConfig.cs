﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FA.Data;

namespace FA.DataAccess.Configuration
{
    public class AttendeeSplittedEntityConfig : EntityTypeConfiguration<Attendee>
    {
        public AttendeeSplittedEntityConfig()
        {
            HasKey(a => a.AttendeeTrackingID);
            Property(a => a.LastName).IsRequired().HasMaxLength(50);
            Map(a =>
            {
                a.Properties(i => new { i.AttendeeTrackingID, i.DateAdded });
                a.ToTable("AttendeeDates");
            });
            Map(a =>
            {
                a.Properties(i => new { i.AttendeeTrackingID, i.LastName });
                a.ToTable("AttendeeNames");
            });
        }
    }
}
