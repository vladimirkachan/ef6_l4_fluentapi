﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FA.Data;

namespace FA.DataAccess.Configuration
{
    public class AttendeeRenameTableConfig : EntityTypeConfiguration<Attendee>
    {
        public AttendeeRenameTableConfig()
        {
            ToTable("Attendees");
            HasKey(a => a.AttendeeTrackingID);
            Property(a => a.AttendeeTrackingID);
            Property(a => a.DateAdded).IsRequired().HasColumnName("X-Data");
        }
    }
}
