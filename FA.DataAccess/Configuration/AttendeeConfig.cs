﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FA.Data;

namespace FA.DataAccess.Configuration
{
    public class AttendeeConfig : EntityTypeConfiguration<Attendee>
    {
        public AttendeeConfig()
        {
            HasKey(attendee => attendee.AttendeeTrackingID);
            Property(attendee => attendee.LastName).IsRequired().HasMaxLength(100);
            Property(attendee => attendee.DateAdded).IsOptional().HasColumnName("Created").HasColumnType("datetime2");
        }
    }
}
